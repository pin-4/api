import http from 'http';
import app from '../app';
import { port } from '../config';
import { pool } from '../database/pgPool';

pool.connect()
  .then()
  .catch((error: any) => console.error(`ERROR: ${error.message || error}`));

app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => {
  console.log(`server running on port ${port}`);
})
  .on('error', (e) => console.error(e));
