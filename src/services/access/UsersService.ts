import Services from '../Service';

const table = 'public.usuarios';
export default class UsersService extends Services {
  private static _intance: UsersService;
  private constructor() {
    super(table);
  }

  public static get instance(): UsersService {
    return this._intance || (this._intance = new this());
  }
}
