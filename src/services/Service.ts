import { FindOptions, UpdateOptions } from '../interfaces/querys';
import { buildSelect, buildInsert, buildUpdate } from '../helpers/querys';
import { pool } from '../database/pgPool';
import * as querys from './querysService';

export default class Services {
  constructor(private table: string) { }

  create = async <T>(o: unknown): Promise<T> => {
    const q = buildInsert(o, this.table);
    const { rows } = await pool.query(q);
    return rows[0] as T;
  }

  update = async (options: UpdateOptions): Promise<any> => {
    const q = buildUpdate(options, this.table);
    const { rows } = await pool.query(q);
    return rows[0];
  }

  findByPk = async (identifier: number | string, options?: { attributes: string[] }): Promise<any> => {
    const q = querys.findByPk(identifier, this.table, options);
    const { rows } = await pool.query(q);
    return rows[0];
  }

  findOne = async (options: FindOptions): Promise<any> => {
    const q = buildSelect(options, this.table);
    const { rows } = await pool.query(q);
    return rows[0];
  }

  findAll = async (options?: FindOptions): Promise<any> => {
    const q = buildSelect(options, this.table);
    const { rows } = await pool.query(q);
    return rows;
  }

  deleteByPk = async (identifier: number | string): Promise<any> => {
    const q = querys.deleteByPk(identifier, this.table);
    const { rows } = await pool.query(q);
    return rows[0];
  }
}
