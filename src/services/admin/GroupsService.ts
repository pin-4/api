import { QueryResult } from 'pg';
import { pool } from '../../database/pgPool';
import Services from '../Service';
import * as query from './querysGroups';

const table = 'public.grupos';
export default class GroupsService extends Services {
  private static _intance: GroupsService;
  private constructor() {
    super(table);
  }

  findAllDetails = (userId?: string): Promise<QueryResult<any>> => {
    let q = query.findAllDetails;
    if (!userId) {
      q = q.replace('where g.usuarioid = $usuarioid', '');
    } else {
      q = q.replace('$usuarioid', userId);
    }
    return pool.query(q);
  }

  public static get instance(): GroupsService {
    return this._intance || (this._intance = new this());
  }
}
