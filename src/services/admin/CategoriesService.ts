import Services from '../Service';

const table = 'public.categorias';
export default class CategoriesService extends Services {
  private static _intance: CategoriesService;
  private constructor() {
    super(table);
  }

  public static get instance(): CategoriesService {
    return this._intance || (this._intance = new this());
  }
}
