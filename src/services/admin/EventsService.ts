import { QueryResult } from 'pg';
import { pool } from '../../database/pgPool';
import Services from '../Service';
import * as query from './querysEvents';

const table = 'public.eventos';
export default class EventsService extends Services {
  private static _intance: EventsService;
  private constructor() {
    super(table);
  }

  findAllDetails = async (userId?: string): Promise<QueryResult<any>> => {
    let q = query.findAllDetails;
    if (!userId) {
      q = q.replace('where e.usuarioid = $usuarioid', '');
    } else {
      q = q.replace('$usuarioid', userId);
    }
    return pool.query(q);
  }

  public static get instance(): EventsService {
    return this._intance || (this._intance = new this());
  }
}
