export const findAllDetails = `select g.id, g.nombre, g.descripcion, g.url, g.imagen,
  COALESCE((SELECT row_to_json(t) FROM (
    SELECT id, nombre, slug FROM categorias c
    where c.id = g.categoriaid 
  ) t) ,'{}') categoria,
  COALESCE((SELECT row_to_json(t) FROM (
    SELECT id, nombre, imagen, descripcion, email FROM usuarios u
    where u.id = g.usuarioid
  ) t) ,'{}') usuario
  from grupos g
  where g.usuarioid = $usuarioid`;
