export const findAllDetails = `select e.id, e.titulo, e.slug, e.invitado, e.cupo, e.descripcion, 
  e.fecha, e.hora, e.descripcion, e.ciudad, e.estado, e.pais,
  e.ubicacion,e.interesados, e.asistencia,
  COALESCE((SELECT array_to_json(array_agg(row_to_json(t))) FROM (
    SELECT c.id, c.mensaje,
    COALESCE((SELECT row_to_json(t) FROM (
      SELECT id, nombre, imagen
      FROM usuarios u
      where u.id = c.usuarioid
    ) t) ,'{}') usuario
    FROM comentarios c
    where c.eventoid = e.id
  ) t) ,'[]') comentarios,
  COALESCE((SELECT row_to_json(t) FROM (
    SELECT g.id, g.nombre, g.descripcion, g.url, g.imagen,
    COALESCE((SELECT row_to_json(t) FROM (
      SELECT id, nombre, slug 
      FROM categorias c
      where c.id = g.categoriaid 
  ) t) ,'{}') categoria
    FROM grupos g
    where g.id = e.grupoid
  ) t) ,'{}') grupo,
  COALESCE((SELECT row_to_json(t) FROM (
    SELECT id, nombre, imagen, descripcion, email 
    FROM usuarios u
    where u.id = e.usuarioid
  ) t) ,'{}') usuario
  from eventos e
  where e.usuarioid = $usuarioid`;
