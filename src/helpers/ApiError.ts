import { Response } from 'express';
import { ResponseStatus } from '../interfaces/ApiResponse';
import { StatusCode } from '../interfaces/ApiResponse';

enum ErrorType {
  BAD_TOKEN = 'BadTokenError',
  TOKEN_EXPIRED = 'TokenExpiredError',
  UNAUTHORIZED = 'AuthFailureError',
  ACCESS_TOKEN = 'AccessTokenError',
  INTERNAL = 'InternalError',
  NOT_FOUND = 'NotFoundError',
  NO_ENTRY = 'NoEntryError',
  NO_DATA = 'NoDataError',
  BAD_REQUEST = 'BadRequestError',
  FORBIDDEN = 'ForbiddenError'
}

enum ErrorMessage {
  BAD_TOKEN = 'BadTokenError',
  TOKEN_EXPIRED = 'TokenExpiredError',
  UNAUTHORIZED = 'Authentication failure',
  ACCESS_TOKEN = 'AccessTokenError',
  INTERNAL = 'InternalError',
  NOT_FOUND = 'Not Found',
  NO_ENTRY = 'NoEntryError',
  NO_DATA = 'NoDataError',
  BAD_REQUEST = 'Bad Request',
  FORBIDDEN = 'ForbiddenError',
}

/*export class ForbiddenError extends ApiError {
  constructor(message = 'Permission denied') {
    super(ErrorType.FORBIDDEN, message);
  }
}

export class NoEntryError extends ApiError {
  constructor(message = "Entry don't exists") {
    super(ErrorType.NO_ENTRY, message);
  }
}

export class NoDataError extends ApiError {
  constructor(message = 'No data available') {
    super(ErrorType.NO_DATA, message);
  }
}*/

export const NotFoundError = (res: Response): Response => {
  return responseError(ResponseStatus.NOT_FOUND, ErrorMessage.NOT_FOUND, res);
};

export const BadRequestError = (message: string, res: Response): Response => {
  return responseError(ResponseStatus.BAD_REQUEST, message, res);
};

export const InternalError = (res: Response): Response => {
  return responseError(ResponseStatus.INTERNAL_ERROR, ErrorType.INTERNAL, res);
};

export const AuthFailureError = (res: Response, message: string = ErrorMessage.UNAUTHORIZED): Response => {
  return responseError(ResponseStatus.UNAUTHORIZED, message, res);
};

const responseError = (status: number, message: string, res: Response): Response => {
  return res.status(status).json({
    statusCode: StatusCode.FAILURE,
    message
  });
};
