import { FindOptions, UpdateOptions } from "../interfaces/querys";

export const buildSelect = (options: FindOptions, table: string): string => {
  let attributes: any = undefined;
  let where: any = undefined;
  if (options) {
    attributes = options.attributes;
    where = options.where;
  }
  let whereBuilt = '';
  if (where) {
    whereBuilt = 'WHERE ';
    Object.entries(where).forEach(([key, value]) => {
      const dato = (typeof value === 'string' ? `= '${value}'` : (value === null) ? ' IS NULL' : `=${value}`);
      whereBuilt += key + dato + ' AND ';
    });
    whereBuilt = whereBuilt.slice(0, (whereBuilt.length - 4));
  }

  const q = `SELECT ${(attributes === undefined) ? '*' : attributes.toString()} 
    FROM ${table}
    ${whereBuilt}`;
  return q;
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const buildInsert = (o: any, table: string): string => {
  let keys = '';
  let values = '';

  Object.entries(o).forEach(([key, value]) => {
    keys += `${key},`;
    values += `${(typeof value === 'string' ? `'${value}'` : value)},`;
  });
  keys = keys.slice(0, (keys.length - 1));
  values = values.slice(0, (values.length - 1));

  const q = `INSERT INTO ${table}
  (${keys}) 
  VALUES (${values}) RETURNING *;`;
  return q;
};

export const buildUpdate = (options: UpdateOptions, table: string): string => {
  delete options.o.id;
  let whereBuilt = 'WHERE ';
  Object.entries(options.where).forEach(([key, value]) => {
    const dato = (typeof value === 'string' ? `= '${value}'` : (value === null) ? ' IS NULL' : `=${value}`);
    whereBuilt += key + dato + ' AND ';
  });
  whereBuilt = whereBuilt.slice(0, (whereBuilt.length - 4));

  let values = '';
  Object.entries(options.o).forEach(([key, value]) => {
    console.log(key, value);
    values += `${key}=${(typeof value === 'string') ? `'${value}'` : value}, `;
  });
  values = values.slice(0, (values.length - 2));

  const q = `UPDATE ${table}
  SET ${values}
  ${whereBuilt} RETURNING *;`;
  return q;
};
