export const confirmAccount = (url: string): string => /*html*/`
  <h1 style="
      text-align: center;
      font-family: Arial, Helvetica;
  ">Reestablecer Password</h1>
  <p style="font-family: Arial, Helvetica;">Hola, has solicitado reestablecer tu password, haz click en el siguiente enlace para reestablecerlo, este enlace es temporal, asi que si se vence vuelve a solicitarlo</p>

  <a style="
      display: block;
      font-family: Arial, Helvetica;
      padding: 1rem;
      background-color: #00C897;
      color: white;
      text-transform: uppercase;
      text-align: center;
      text-decoration: none;
  " href="${url}">Ir a Resetear el Password</a>

  <p style="font-family: Arial, Helvetica;">Si no puedes acceder a este enlace, vísita : ${url}</p>

  <p style="font-family: Arial, Helvetica;">Si no solicitaste este e-mail, puedes ignorarlo</p>
`;
