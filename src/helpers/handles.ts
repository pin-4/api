export const handleError = (msg = 'ERROR', e: Error | string = ''): void => {
  console.log(msg, e);
};

export const handleSuccess = (msg = 'SUCCESS'): void => {
  console.log(msg);
};
