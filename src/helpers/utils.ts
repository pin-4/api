export default {
  pick: (o: any, propiedades: any[]) => {
    return JSON.parse(JSON.stringify(o, propiedades));
  }
};
