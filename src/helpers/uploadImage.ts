import { v4 as uuidv4 } from 'uuid';
import { join } from 'path';
import { existsSync, mkdirSync, writeFile } from 'fs';

export const uploadImage = (encoded: string, folder: string): Promise<string> => new Promise((resolve, rejects) => {
  const base64Image: any = encoded.split(';base64,').pop();
  const image = `${uuidv4()}.png`;

  const ruta: string = join(__dirname, `../uploads/${folder}`);

  if (!existsSync(ruta)) mkdirSync(ruta, { recursive: true });

  writeFile(`${ruta}/${image}`, base64Image, { encoding: 'base64' }, function (err: any) {
    if (err) rejects(err);
    resolve(image);
  });
});
