export interface Iuser {
  name: string,
  email: string,
  password: string,
  type?: number
}

export interface Isession {
  email: string;
  nombre: string;
  id: number;
  iat: number;
  exp: number;
}
