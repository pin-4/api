import { Isession } from "./Iuser";

export interface IEventWS extends IEvent {
  session: Isession
}

export interface IEvent extends IEventBasic {
  slug: string;
}
export interface IEventDB extends IEventBasic, IEvent {
  id: string;
  invitado?: any;
  cupo: number;
  estado?: any;
  asistencia?: any;
  createdat?: any;
  updatedat?: any;
  interesados?: any;
}

interface IEventBasic {
  titulo: string;
  descripcion: string;
  fecha: string;
  hora: string;
  direccion: string;
  ciudad: string;
  pais: string;
  grupoid: string;
  ubicacion: string;
  usuarioid?: number;
}
