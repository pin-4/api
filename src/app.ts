import express, { Request, Response, NextFunction, Application } from 'express';
import compression from 'compression';
import helmet from 'helmet';
import cors from 'cors';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import { join } from 'path';

import routesV1 from './routes/v1';
import { NotFoundError } from './helpers/ApiError';

const app: Application = express();

app.use(cors());
app.use(helmet());
app.use(compression());

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(morgan('dev'));

app.use(express.static(join(__dirname, 'uploads')));

app.use('/v1', routesV1);

// catch 404 and forward to error handler
app.use((_req: Request, res: Response, next: NextFunction) => next(NotFoundError(res)));

export default app;
