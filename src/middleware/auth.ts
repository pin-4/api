import { Request, Response, NextFunction, Router } from 'express';
import jwt from 'jsonwebtoken';
import validator, { ValidationSource } from '../helpers/validator';

import { secretKey } from '../config';
import { AuthFailureError } from '../helpers/ApiError';
import schema from './schema';

const router = Router();

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default router.use(validator(schema.headers, ValidationSource.HEADER), async (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.get('Authorization');

  const token = authHeader.split(' ')[1];
  let revisarToken;
  try {
    revisarToken = jwt.verify(token, secretKey);
  } catch (error) {
    if (String(error).indexOf('invalid token') !== -1) return next(AuthFailureError(res, 'Token is not valid'));
    if (String(error).indexOf('jwt expired') !== -1) return next(AuthFailureError(res, 'Token is expired'));
    return next(AuthFailureError(res));
  }

  if (!revisarToken) return next(AuthFailureError(res, 'Invalid access token'));
  req.body.session = jwt.decode(token);
  next();
});
