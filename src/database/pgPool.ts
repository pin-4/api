import { Pool, PoolConfig, } from 'pg';
import { pgDatabase, pgHost, pgPassword, pgPort, pgSsl, pgUser } from '../config';

const poolConfig: PoolConfig = {
  user: pgUser,
  password: pgPassword,
  host: pgHost,
  database: pgDatabase,
  port: pgPort,
  max: 20,
  ssl: {
    rejectUnauthorized: pgSsl
  }
};

export const pool: Pool = new Pool(poolConfig);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
pool.on('connect', (_client: any): void => {
  console.info('Connection has been established successfully.');
});

pool.on('error', (error: Error) => {
  console.error('Unable to connect to the database:', error);
});
