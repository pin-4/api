import express, { Request, Response, NextFunction } from 'express';
import slug from 'slug';
import { v4 as uuidv4 } from 'uuid';
import _ from '../../../../helpers/utils';
import { SuccessResponse } from '../../../../helpers/ApiResponse';
import { BadRequestError, InternalError } from '../../../../helpers/ApiError';
import EventsService from '../../../../services/admin/EventsService';
import { IEvent, IEventDB, IEventWS } from '../../../../interfaces/Ievent';
import { handleError } from '../../../../helpers/handles';
import validator from '../../../../helpers/validator';
import schema from './schema';

const router = express.Router();
const Events = EventsService.instance;

router.post('/', validator(schema.postEvent), async (req: Request, res: Response) => {
  try {
    const body = <IEventWS>{ ...req.body };
    // const point = { type: 'Point', coordinates: [parseFloat(req.body.lat), parseFloat(req.body.lng)] }
    // body.ubicacion = point;
    body.usuarioid = body.session.id;
    delete body.session;

    
    const url = slug(body.titulo).toLowerCase();
    const data: IEvent = { ...body, slug: `${url}-${uuidv4()}` };

    const event = await Events.create<IEventDB>({ id: uuidv4(), ...data });

    return SuccessResponse(res, 'Event created successfully', {
      event: _.pick(event, ['id'])
    });
  } catch (error) {
    handleError('Error post event', error);
    return InternalError(res);
  }
});

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const result = await Events.findAllDetails();

    SuccessResponse(res, 'Events obtained', result.rows).send(res);
  } catch (error) {
    console.log(error);
    next(InternalError(res));
  }
});

router.get('/:groupId', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const group = await Events.findOne({
      where: {
        id: req.params.groupId,
        usuarioid: req.body.session.id
      }
    });
    if (!group) return next(BadRequestError('Invalid group', res));

    SuccessResponse(res, 'Group obtained', {
      group: _.pick(group, ['id', 'nombre', 'descripcion', 'url', 'imagen', 'categoriaid', 'usuarioid'])
    }).send(res);
  } catch (err) {
    if (!err.toString().indexOf('error: la sintaxis de entrada')) return next(BadRequestError('Invalid group', res));
    next(InternalError(res));
  }
});


router.put('/:groupId', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { name, description, categoryId } = req.body;

    const group = await Events.findOne({
      where: {
        id: req.params.groupId,
        usuarioid: req.body.session.id
      }
    });
    if (!group) return next(BadRequestError('Invalid group', res));

    group.nombre = name;
    group.descripcion = description;
    group.categoriaid = categoryId;

    const newGroup = await Events.update({
      o: group,
      where: { id: group.id }
    });

    SuccessResponse(res, 'Group updated successfully', {
      group: _.pick(newGroup, ['id'])
    }).send(res);
  } catch (err) {
    if (!err.toString().indexOf('error: la sintaxis de entrada')) return next(BadRequestError('Invalid group', res));
    next(InternalError(res));
  }
});

export default router;
