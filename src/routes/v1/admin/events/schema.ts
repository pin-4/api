import Joi from 'joi';

export default {
  postEvent: Joi.object().keys({
    titulo: Joi.string().required().min(3),
    descripcion: Joi.string().required().min(3),
    fecha: Joi.date().required(),
    hora: Joi.string().required(),
    direccion: Joi.string().required(),
    ciudad: Joi.string().required(),
    pais: Joi.string().required(),
    grupoid: Joi.string().required(),
    ubicacion: Joi.string().required(),
    session: Joi.object().required()
  })
};
