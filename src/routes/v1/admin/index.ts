import { Router } from 'express';
import groups from './groups/groups';
import events from './events/events';

const router = Router();

router.use('/groups', groups);
router.use('/events', events);

export default router;
