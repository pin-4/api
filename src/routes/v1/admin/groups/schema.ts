import Joi from 'joi';

export default {
  createGroup: Joi.object().keys({
    name: Joi.string().required().min(3),
    description: Joi.string().required().min(3),
    categoryId: Joi.number().required().min(1),
    image: Joi.optional(),
    session: Joi.object().required(),
    url: Joi.optional()
  }),
  updateGroup: Joi.object().keys({
    name: Joi.string().required().min(3),
    description: Joi.string().required().min(3),
    categoryId: Joi.number().required().min(1),
    image: Joi.optional(),
    session: Joi.object().required(),
    url: Joi.optional()
  })
};
