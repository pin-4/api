import { Request, Response, NextFunction, Router } from 'express';
import { v4 as uuidv4 } from 'uuid';
import _ from '../../../../helpers/utils';
import { SuccessResponse } from '../../../../helpers/ApiResponse';
import { BadRequestError, InternalError } from '../../../../helpers/ApiError';
import { uploadImage } from '../../../../helpers/uploadImage';
import GroupsService from '../../../../services/admin/GroupsService';
import validator from '../../../../helpers/validator';
import schema from './schema';

const router = Router();
const Groups = GroupsService.instance;


router.post('/', validator(schema.createGroup), async (req: Request, res: Response, next: NextFunction) => {
  try {
    req.body.url = req.body.url || null;
    req.body.image = req.body.image || null;

    if (req.body.image) {
      try {
        req.body.image = await uploadImage(req.body.image, 'groups');
      } catch (error) {
        console.log(error);
        req.body.image = null;
      }
    }

    const group = await Groups.create({
      id: uuidv4(),
      nombre: req.body.name,
      descripcion: req.body.description,
      url: req.body.url,
      imagen: req.body.image,
      categoriaId: req.body.categoryId,
      usuarioId: req.body.session.id
    });

    SuccessResponse(res, 'Group created successfully', {
      group: _.pick(group, ['id'])
    });
  } catch (error) {
    console.log(error);
    next(InternalError(res));
  }
});

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const usuarioId = req.body.session.id;

    const result = await Groups.findAllDetails(usuarioId);

    SuccessResponse(res, 'Groups obtained', result.rows);
  } catch (error) {
    console.log(error);
    next(InternalError(res));
  }
});

router.get('/:groupId', async (req: Request, res: Response) => {
  try {
    const group = await Groups.findOne({
      where: {
        id: req.params.groupId,
        usuarioid: req.body.session.id
      }
    });
    if (!group) return BadRequestError('Invalid group', res);

    SuccessResponse(res, 'Group obtained', _.pick(group, ['id', 'nombre', 'descripcion', 'url', 'imagen', 'categoriaid', 'usuarioid'])
    );
  } catch (err) {
    if (!err.toString().indexOf('error: la sintaxis de entrada')) return BadRequestError('Invalid group', res);
    InternalError(res);
  }
});

router.put('/:groupId', validator(schema.updateGroup), async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { name, description, categoryId } = req.body;

    const group = await Groups.findOne({
      where: {
        id: req.params.groupId,
        usuarioid: req.body.session.id
      }
    });
    if (!group) return next(BadRequestError('Invalid group', res));

    const grupo = {
      nombre: name,
      descripcion: description,
      categoriaid: categoryId
    };
    const newGroup = await Groups.update({
      o: grupo,
      where: { id: group.id }
    });

    SuccessResponse(res, 'Group updated successfully', {
      group: _.pick(newGroup, ['id'])
    });
  } catch (err) {
    console.log(err);
    if (!err.toString().indexOf('error: la sintaxis de entrada')) return next(BadRequestError('Invalid group', res));
    next(InternalError(res));
  }
});

router.delete('/:groupId', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { groupId } = req.params;

    const group = await Groups.deleteByPk(groupId);
    if (!group) return next(BadRequestError('Invalid group', res));

    SuccessResponse(res, 'Deleted Group', {
      group: _.pick(group, ['id', 'nombre', 'descripcion', 'url', 'imagen', 'categoriaid', 'usuarioid'])
    });
  } catch (err) {
    if (!err.toString().indexOf('error: la sintaxis de entrada')) return next(BadRequestError('Invalid group', res));
    next(InternalError(res));
  }
});

export default router;
