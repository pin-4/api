import express, { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { SuccessResponse } from '../../../helpers/ApiResponse';
import _ from '../../../helpers/utils';
import { secretKey } from '../../../config';
import { AuthFailureError, BadRequestError } from '../../../helpers/ApiError';
import UsersService from '../../../services/access/UsersService';
import validator from '../../../helpers/validator';
import schema from './schema';

const router = express.Router();
const Users = UsersService.instance;

export default router.post('/', validator(schema.userCredential), async (req: Request, res: Response, next: NextFunction) => {
  const { email, password } = req.body;

  const user = await Users.findOne({
    attributes: ['id', 'nombre', 'password', 'email'],
    where: {
      email
    }
  });

  if (!user) return next(BadRequestError('User not registered', res));

  const match = await bcrypt.compare(password, user.password);
  if (!match) return next(AuthFailureError(res));

  const token = jwt.sign({
    email: user.email,
    nombre: user.nombre,
    id: user.id
  }, secretKey, { expiresIn: '24h' });
  SuccessResponse(res, 'Sign in Success', {
    user: _.pick(user, ['id', 'nombre', 'email']),
    tokens: token
  });
});
