import { Request, Response, NextFunction, RequestHandler, Router } from 'express';
import bcrypt from 'bcrypt';
import { SendMailOptions } from 'nodemailer';

import schema from './schema';

import _ from '../../../helpers/utils';
import UsersService from '../../../services/access/UsersService';
import validator from '../../../helpers/validator';
import { BadRequestError, InternalError } from '../../../helpers/ApiError';
import { SuccessResponse } from '../../../helpers/ApiResponse';
import { SendEmail } from '../../../jobs/email';
import { Iuser } from '../../../interfaces/Iuser';
import { confirmAccountHtml, confirmAccountText } from '../../../helpers/templates/emails/confirmAccount';
import { urlClient } from '../../../config';

const router = Router();
const Users = UsersService.instance;

const validateEmailUser: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = await Users.findOne({
      attributes: ['email'],
      where: {
        email: req.body.email
      }
    });

    if (user) {
      next(BadRequestError('User already registered', res));
    }
    next();
  } catch (error) {
    console.log(error);
    next(InternalError(res));
  }
};

const createUser: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const passwordHash = await bcrypt.hash(req.body.password, 10);

    req.body.type = req.body.type || 0;
    req.body.password = passwordHash;

    const user: Iuser = await Users.create({
      nombre: req.body.name,
      email: req.body.email,
      password: req.body.password,
      tipo: req.body.type
    });

    const url = `${urlClient}/jkdfsgkjfgbdsfjkagh`;
    const optionsEmail: SendMailOptions = {
      to: user.email,
      subject: 'Confirma tu cuenta de workshops',
      text: confirmAccountText(url),
      html: confirmAccountHtml(url)
    };

    SendEmail(optionsEmail);

    SuccessResponse(res, 'Signup Successful', {
      user: _.pick(user, ['id'])
    });
  } catch (error) {
    console.log(error);
    next(InternalError(res));
  }
};

export default router.post('/', validator(schema.signup), validateEmailUser, createUser);
