import { Request, Response, Router } from 'express';
import { SuccessResponse } from '../../../helpers/ApiResponse';

const router = Router();

export default router.get('/', async (req: Request, res: Response) => {
  SuccessResponse(res, 'Valid token');
});
