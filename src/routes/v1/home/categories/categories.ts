import express, { Request, Response, NextFunction } from 'express';
import { SuccessResponse } from '../../../../helpers/ApiResponse';
import { InternalError } from '../../../../helpers/ApiError';
import CategoriesService from '../../../../services/admin/CategoriesService';

const router = express.Router();
const Categories = CategoriesService.instance;


router.get('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const result = await Categories.findAll();

    SuccessResponse(res, 'Categories obtained', result);
  } catch (error) {
    console.log(error);
    next(InternalError(res));
  }
});

export default router;
