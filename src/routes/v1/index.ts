import express from 'express';
import test from './tests/test';
import signup from './access/signup';
import signin from './access/signin';
import categories from './home/categories/categories';
import admin from './admin';
import auth from '../../middleware/auth';
import home from './home/home';

const router = express.Router();

router.use('/test', test);
router.use('/signup', signup);
router.use('/signin', signin);
router.use('/categories', categories);
router.use('/admin', auth, admin);
router.use('/session', auth, home);

export default router;
