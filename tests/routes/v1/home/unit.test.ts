import supertest from 'supertest';

import app from '../../../../src/app';

describe('welcome to Workshops api', () => {
  const endpoint = '/';
  const request = supertest(app);

  it('Home', async (done) => {
    const response = await request.get(endpoint);
    expect(response.status).toBe(200);
    done();
  });
});
